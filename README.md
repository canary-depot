# README #

[![CircleCI](https://circleci.com/bb/ccinnamon/canary.svg?style=svg&circle-token=1b75771db69134694023aba04a16069ecdb0d204)](https://circleci.com/bb/ccinnamon/canary)[![Dependency Status](https://www.versioneye.com/user/projects/588a68ddc64626004e0578a4/badge.svg?style=flat-square)](https://www.versioneye.com/user/projects/588a68ddc64626004e0578a4)

This is the readme file, for canary.

### What is canary ###

Canary is a project management webapp written in Spring.

### How do I get set up? ###

These are instructions of how to build and run canary.

## Dependencies ##

+ Postgresql (for the database);
+ Postgis (for geographic information);
+ Maven (in order to build);

## Configuration ##

In order to specify the databse to use, you have to modify the file in:

    ./src/main/resources/META-INF/database.properties

## Build ##


In order to build and create the needed tables, using maven run:

    mvn -Pmigrate-db package

In order to create the specified database and the needed tables and build a war file.

If you already have the tables, using maven run:

    mvn package

After the build, go to:

    ./target/canary-<version>.war

Where version is the current version of canary, and deploy the war file to your favorite Application Server,
Apache Tomcat or Eclipse Jetty will work just fine.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact