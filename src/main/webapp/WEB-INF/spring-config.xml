<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:cache="http://www.springframework.org/schema/cache"
       xmlns:context="http://www.springframework.org/schema/context"
       xmlns:jdbc="http://www.springframework.org/schema/jdbc"
       xmlns:mvc="http://www.springframework.org/schema/mvc"
       xmlns:tx="http://www.springframework.org/schema/tx"
       xmlns:util="http://www.springframework.org/schema/util"
       xmlns:encryption="http://www.jasypt.org/schema/encryption"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd
		http://www.springframework.org/schema/cache http://www.springframework.org/schema/cache/spring-cache.xsd
		http://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context-4.3.xsd
		http://www.springframework.org/schema/jdbc http://www.springframework.org/schema/jdbc/spring-jdbc-4.3.xsd
		http://www.springframework.org/schema/mvc http://www.springframework.org/schema/mvc/spring-mvc-4.3.xsd
		http://www.springframework.org/schema/tx http://www.springframework.org/schema/tx/spring-tx-4.3.xsd
		http://www.springframework.org/schema/util http://www.springframework.org/schema/util/spring-util-4.3.xsd
    http://www.jasypt.org/schema/encryption http://www.jasypt.org/schema/encryption/jasypt-spring31-encryption-1.xsd">

  <!-- All of our beans will be inside cc.cinnamon.canary package... -->
  <context:component-scan base-package="cc.cinnamon.canary"/>

  <!-- ...and it will be annotation driven -->
  <mvc:annotation-driven/>

  <beans>

    <!--
      We use two external property files to configure certain things, like database properties, since
      we can eventually use more than one database type, and configure that inside our build.gradle script...
     -->
    <bean class="org.springframework.beans.factory.config.PropertyPlaceholderConfigurer">
      <property name="locations">
        <list>
          <value>classpath:/META-INF/database.properties</value>
          <value>classpath:/META-INF/logging.properties</value>
        </list>
      </property>
    </bean>

    <!--
      Class used to connect logback to spring, so we can configure logback inside spring.

      NOTE: That we may eventually remove this logging configuration and use the plain logback.xml
      file, if needed...
     -->
    <bean class="ch.qos.logback.ext.spring.ApplicationContextHolder"/>

    <bean id="consoleAppender" class="ch.qos.logback.core.ConsoleAppender" init-method="start" destroy-method="stop">
      <property name="context" value="#{ T(org.slf4j.LoggerFactory).getILoggerFactory() }"/>
      <property name="encoder">
        <bean class="ch.qos.logback.classic.encoder.PatternLayoutEncoder" init-method="start" destroy-method="stop">
          <property name="context" value="#{ T(org.slf4j.LoggerFactory).getILoggerFactory() }"/>
          <property name="pattern" value="%date %-5level [%thread] %logger{36} %m%n"/>
        </bean>
      </property>
    </bean>

    <!--
      This is our SQL Data Source Connection Pool, so that we use
      a maximum of connections. Similar to a thread pool.
    -->
    <bean id="hikariConfiguration" class="com.zaxxer.hikari.HikariConfig">
      <property name="autoCommit" value="false"/>
      <property name="dataSourceClassName" value="${datasource.classnameHikari}"/>
      <property name="poolName" value="springHikariCP" />
      <property name="connectionTestQuery" value="SELECT 1" />
      <property name="maximumPoolSize" value="${datasource.maxpoolsize}" />
      <property name="idleTimeout" value="${datasource.idleTimeout}" />

      <property name="dataSourceProperties">
        <props>
          <prop key="url">${dataSource.url}</prop>
          <prop key="user">${dataSource.username}</prop>
          <prop key="password">${dataSource.password}</prop>
        </props>
      </property>
    </bean>

    <bean id="dataSource" destroy-method="close" class="com.zaxxer.hikari.HikariDataSource">
      <constructor-arg name="configuration" ref="hikariConfiguration"/>
    </bean>

    <!--
      In order to make the pooled data source, transaction aware...
    -->
    <bean id="transactionAwareDataSource"
          class="org.springframework.jdbc.datasource.TransactionAwareDataSourceProxy">
      <constructor-arg name="targetDataSource" ref="dataSource"/>
    </bean>

    <!-- Spring transaction manager -->
    <bean id="transactionManager"
          class="org.springframework.jdbc.datasource.DataSourceTransactionManager">
      <constructor-arg name="dataSource" ref="dataSource"/>
    </bean>

    <!--
      The jooq connection provider, that uses our transaction
      aware pooled data source.
    -->
    <bean id="connectionProvider"
          class="org.jooq.impl.DataSourceConnectionProvider">
      <constructor-arg name="dataSource" ref="transactionAwareDataSource"/>
    </bean>

    <!--
      The default configuration for jooq
     -->
    <bean id="defaultConfiguration"
          class="org.jooq.impl.DefaultConfiguration">
      <property name="connectionProvider" ref="connectionProvider"/>
      <property name="SQLDialect" value="${dataSource.sqlDialeact}"/>
    </bean>

    <!-- The DSLContext that jooq will use -->
    <bean id="defaultContext"
          class="org.jooq.impl.DefaultDSLContext">
      <constructor-arg name="configuration"
                       ref="defaultConfiguration"/>
    </bean>

    <bean name="/applicationSwaggerConfig" class="cc.cinnamon.canary.config.ApplicationSwaggerConfig"/>
  </beans>

</beans>
