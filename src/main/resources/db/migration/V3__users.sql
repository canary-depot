create table users (
  id bigserial primary key,
  name varchar(200) not null,
  password varchar(512) not null,
  email varchar(254) check ( email ~* '[A-Z0-9._%-]+@[A-Z0-9._%-]+\.[A-Z]{2,4}' ),
  age int default 18,
  city varchar(122),
  location point3d,
  birthday date,
  created_stamp timestamp not null default now(),
  active boolean not null default true
);

create table roles (
  id bigserial primary key,
  name varchar(10) not null,
  description text
);

create table permissions (
  id bigserial primary key,
  name varchar(10) not null,
  description text
);

create table users_roles (
  id bigserial primary key,
  user_id bigint not null references users(id),
  role_id bigint not null references roles(id)
);

create table roles_permissions (
  id bigserial primary key,
  role_id bigint not null references roles(id),
  permission_id bigint not null references permissions(id)
);