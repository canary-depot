create type point as (
	latitude numeric( 10, 6 ),
	longitude numeric( 10, 6 )
);

create type point3d as (
	latitude numeric( 10, 6),
	longitude numeric( 10, 6 ),
	altitude numeric( 8, 3 )
);
