create table project (
	id bigserial primary key,
	name varchar(200) not null unique,
	description text,
	homepage varchar(120),
	created date,
	shortdesc varchar(200),
	category varchar(60),
	language varchar(20),
	license_id bigint
);

create table license(
	id bigserial primary key,
	name varchar(10),
	url varchar(120),
	description text
);

alter table project add foreign key( license_id ) references license(id);
