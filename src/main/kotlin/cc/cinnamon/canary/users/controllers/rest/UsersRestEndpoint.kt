package cc.cinnamon.canary.users.controllers.rest

import cc.cinnamon.canary.users.model.tables.pojos.Roles
import cc.cinnamon.canary.users.model.tables.pojos.Users
import cc.cinnamon.canary.users.repository.UsersRepository
import cc.cinnamon.canary.utilities.exceptions.DuplicatedResource
import cc.cinnamon.canary.utilities.exceptions.InvalidRequestParameter
import cc.cinnamon.canary.utilities.exceptions.ResourceNotFound
import cc.cinnamon.canary.utilities.rest.RestEnvelope
import cc.cinnamon.canary.utilities.rest.RestListEnvelope
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.*

import javax.validation.Valid
import java.io.Serializable
import java.util.ArrayList

/**
 * @author carddammom
 */
@RestController
@RequestMapping(path = arrayOf("/rs/users"), produces = arrayOf(MediaType.APPLICATION_JSON_UTF8_VALUE), consumes = arrayOf(MediaType.APPLICATION_JSON_UTF8_VALUE))
class UsersRestEndpoint @Autowired
constructor(private val users: UsersRepository) : Serializable {

    @RequestMapping(method = arrayOf(RequestMethod.GET))
    @Transactional
    fun getUsers(): RestListEnvelope<Users> {
        val allUsers = ArrayList<Users>()
        this.users.findAll()!!.forEach { user -> allUsers.add(user) }
        return RestListEnvelope.builder<Users>().withData(allUsers).withErrorCode(200).withErrorMessage("OK")
                .build()
    }

    @RequestMapping(path = arrayOf("/{id}"), method = arrayOf(RequestMethod.GET))
    @Transactional
    @Throws(ResourceNotFound::class)
    fun getUser(@PathVariable("id") userId: Long?): RestEnvelope<Users> {
        val user = this.users.findOne(userId) ?: throw ResourceNotFound(userId)

        return RestEnvelope.builder<Users>().withData(user).withErrorCode(200).withErrorMessage("OK").build()
    }

    @RequestMapping(method = arrayOf(RequestMethod.POST))
    @Transactional
    @Throws(InvalidRequestParameter::class, DuplicatedResource::class)
    fun saveUser(@RequestBody @Valid user: Users?): RestEnvelope<Void> {
        if (user == null) {
            throw InvalidRequestParameter()
        }

        if (user.id != null && this.users.exists(user.id)) {
            throw DuplicatedResource()
        }

        user.id = null
        this.users.save(user)

        return RestEnvelope.builder<Void>().withData(null).withErrorCode(201).withErrorMessage("Created").build()
    }

    @RequestMapping(method = arrayOf(RequestMethod.PUT))
    @Transactional
    @Throws(ResourceNotFound::class, InvalidRequestParameter::class)
    fun updateUser(@RequestBody @Valid user: Users?): RestEnvelope<Void> {
        if (user == null) {
            throw InvalidRequestParameter()
        }

        if (user.id == null || !this.users.exists(user.id)) {
            throw ResourceNotFound(user.id)
        }

        this.users.save(user)

        return RestEnvelope.builder<Void>().withData(null).withErrorCode(200).withErrorMessage("OK").build()
    }

    @RequestMapping(method = arrayOf(RequestMethod.DELETE))
    @Transactional
    @Throws(ResourceNotFound::class)
    fun deleteUser(@PathVariable("id") userId: Long?): RestEnvelope<Void> {
        val user = this.users.findOne(userId) ?: throw ResourceNotFound(userId)

        return RestEnvelope.builder<Void>().withData(null).withErrorCode(200).withErrorMessage("OK").build()
    }

    @RequestMapping(path = arrayOf("/{id}/roles"), method = arrayOf(RequestMethod.GET))
    @Transactional
    @Throws(ResourceNotFound::class)
    fun getUserRoles(@PathVariable("id") userId: Long?): RestListEnvelope<Roles> {
        val user = this.users.findOne(userId) ?: throw ResourceNotFound(userId)

        val userRoles = ArrayList<Roles>()
        return RestListEnvelope.builder<Roles>().withData(userRoles).withErrorCode(200).withErrorMessage("OK")
                .build()
    }

    @RequestMapping(path = arrayOf("/{id}/roles"), method = arrayOf(RequestMethod.PUT))
    @Transactional
    @Throws(ResourceNotFound::class)
    fun saveUserRoles(@PathVariable("id") userId: Long?,
                      @RequestBody @Valid userRoles: List<Roles>): RestEnvelope<Void>? {

        val user = this.users.findOne(userId) ?: throw ResourceNotFound(userId)

        return null
    }

    companion object {

        private const val serialVersionUID = 6876551614770778685L
    }

}
