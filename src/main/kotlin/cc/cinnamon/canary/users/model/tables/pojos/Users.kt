package cc.cinnamon.canary.users.model.tables.pojos

import cc.cinnamon.canary.users.model.tables.interfaces.IUsers

import javax.validation.constraints.NotNull
import javax.validation.constraints.Size
import java.sql.Date
import java.sql.Timestamp

class Users : IUsers {

    override var id: Long? = null
    @get:NotNull
    @get:Size(max = 200)
    override var name: String? = null
    @get:NotNull
    @get:Size(max = 512)
    override var password: String? = null
    @get:Size(max = 254)
    override var email: String? = null
    override var age: Int? = null
    @get:Size(max = 122)
    override var city: String? = null
    override var location: Any? = null
    override var birthday: Date? = null
    override var createdStamp: Timestamp? = null
    override var active: Boolean? = null

    constructor() {}

    constructor(value: Users) {
        this.id = value.id
        this.name = value.name
        this.password = value.password
        this.email = value.email
        this.age = value.age
        this.city = value.city
        this.location = value.location
        this.birthday = value.birthday
        this.createdStamp = value.createdStamp
        this.active = value.active
    }

    constructor(id: Long?, name: String, password: String, email: String, age: Int?, city: String, location: Any,
                birthday: Date, createdStamp: Timestamp, active: Boolean?) {
        this.id = id
        this.name = name
        this.password = password
        this.email = email
        this.age = age
        this.city = city
        this.location = location
        this.birthday = birthday
        this.createdStamp = createdStamp
        this.active = active
    }

    override fun equals(obj: Any?): Boolean {
        if (this === obj) {
            return true
        }
        if (obj == null) {
            return false
        }
        if (this.javaClass != obj.javaClass) {
            return false
        }
        val other = obj as Users?
        if (this.id == null) {
            if (other!!.id != null) {
                return false
            }
        } else if (this.id != other!!.id) {
            return false
        }
        if (this.name == null) {
            if (other.name != null) {
                return false
            }
        } else if (this.name != other.name) {
            return false
        }
        if (this.password == null) {
            if (other.password != null) {
                return false
            }
        } else if (this.password != other.password) {
            return false
        }
        if (this.email == null) {
            if (other.email != null) {
                return false
            }
        } else if (this.email != other.email) {
            return false
        }
        if (this.age == null) {
            if (other.age != null) {
                return false
            }
        } else if (this.age != other.age) {
            return false
        }
        if (this.city == null) {
            if (other.city != null) {
                return false
            }
        } else if (this.city != other.city) {
            return false
        }
        if (this.location == null) {
            if (other.location != null) {
                return false
            }
        } else if (this.location != other.location) {
            return false
        }
        if (this.birthday == null) {
            if (other.birthday != null) {
                return false
            }
        } else if (this.birthday != other.birthday) {
            return false
        }
        if (this.createdStamp == null) {
            if (other.createdStamp != null) {
                return false
            }
        } else if (!this.createdStamp!!.equals(other.createdStamp)) {
            return false
        }
        if (this.active == null) {
            if (other.active != null) {
                return false
            }
        } else if (this.active != other.active) {
            return false
        }
        return true
    }

    override fun hashCode(): Int {
        val prime = 31
        var result = 1
        result = prime * result + if (this.id == null) 0 else this.id!!.hashCode()
        result = prime * result + if (this.name == null) 0 else this.name!!.hashCode()
        result = prime * result + if (this.password == null) 0 else this.password!!.hashCode()
        result = prime * result + if (this.email == null) 0 else this.email!!.hashCode()
        result = prime * result + if (this.age == null) 0 else this.age!!.hashCode()
        result = prime * result + if (this.city == null) 0 else this.city!!.hashCode()
        result = prime * result + if (this.location == null) 0 else this.location!!.hashCode()
        result = prime * result + if (this.birthday == null) 0 else this.birthday!!.hashCode()
        result = prime * result + if (this.createdStamp == null) 0 else this.createdStamp!!.hashCode()
        result = prime * result + if (this.active == null) 0 else this.active!!.hashCode()
        return result
    }

    override fun toString(): String {
        val sb = StringBuilder("Users (")

        sb.append(this.id)
        sb.append(", ").append(this.name)
        sb.append(", ").append(this.password)
        sb.append(", ").append(this.email)
        sb.append(", ").append(this.age)
        sb.append(", ").append(this.city)
        sb.append(", ").append(this.location)
        sb.append(", ").append(this.birthday)
        sb.append(", ").append(this.createdStamp)
        sb.append(", ").append(this.active)

        sb.append(")")
        return sb.toString()
    }

    // -------------------------------------------------------------------------
    // FROM and INTO
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    override fun from(from: IUsers) {
        this.id = from.id
        this.name = from.name
        this.password = from.password
        this.email = from.email
        this.age = from.age
        this.city = from.city
        this.location = from.location
        this.birthday = from.birthday
        this.createdStamp = from.createdStamp
        this.active = from.active
    }

    /**
     * {@inheritDoc}
     */
    override fun <E : IUsers> into(into: E): E {
        into.from(this)
        return into
    }

    companion object {

        private val serialVersionUID: Long = -311582637
    }
}
