/*
 * This file is generated by jOOQ.
*/
package cc.cinnamon.canary.users.model.tables.interfaces


import javax.annotation.Generated
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size
import java.io.Serializable


/**
 * This class is generated by jOOQ.
 */
@Generated(value = *arrayOf("http://www.jooq.org", "jOOQ version:3.9.1", "schema version:public_4"), date = "2017-02-20T21:56:50.783Z", comments = "This class is generated by jOOQ")
interface IRoles : Serializable {

    /**
     * Getter for `public.roles.id`.
     */
    /**
     * Setter for `public.roles.id`.
     */
    var id: Long?

    /**
     * Getter for `public.roles.name`.
     */
    /**
     * Setter for `public.roles.name`.
     */
    @get:NotNull
    @get:Size(max = 10)
    var name: String

    /**
     * Getter for `public.roles.description`.
     */
    /**
     * Setter for `public.roles.description`.
     */
    var description: String

    // -------------------------------------------------------------------------
    // FROM and INTO
    // -------------------------------------------------------------------------

    /**
     * Load data from another generated Record/POJO implementing the common interface IRoles
     */
    fun from(from: IRoles)

    /**
     * Copy data into another generated Record/POJO implementing the common interface IRoles
     */
    fun <E : IRoles> into(into: E): E
}
