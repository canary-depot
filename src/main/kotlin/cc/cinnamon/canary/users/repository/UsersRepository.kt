package cc.cinnamon.canary.users.repository

import cc.cinnamon.canary.users.model.tables.pojos.Users
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Repository

@Repository
class UsersRepository : PagingAndSortingRepository<Users, Long> {

    override fun <S : Users> save(entity: S): S? {
        // TODO Auto-generated method stub
        return null
    }

    override fun <S : Users> save(entities: Iterable<S>): Iterable<S>? {
        // TODO Auto-generated method stub
        return null
    }

    override fun findOne(id: Long?): Users? {
        // TODO Auto-generated method stub
        return null
    }

    override fun exists(id: Long?): Boolean {
        // TODO Auto-generated method stub
        return false
    }

    override fun findAll(): Iterable<Users>? {
        // TODO Auto-generated method stub
        return null
    }

    override fun findAll(ids: Iterable<Long>): Iterable<Users>? {
        // TODO Auto-generated method stub
        return null
    }

    override fun count(): Long {
        // TODO Auto-generated method stub
        return 0
    }

    override fun delete(id: Long?) {
        // TODO Auto-generated method stub

    }

    override fun delete(entity: Users) {
        // TODO Auto-generated method stub

    }

    override fun delete(entities: Iterable<Users>) {
        // TODO Auto-generated method stub

    }

    override fun deleteAll() {
        // TODO Auto-generated method stub

    }

    override fun findAll(sort: Sort): Iterable<Users>? {
        // TODO Auto-generated method stub
        return null
    }

    override fun findAll(pageable: Pageable): Page<Users>? {
        // TODO Auto-generated method stub
        return null
    }

}
