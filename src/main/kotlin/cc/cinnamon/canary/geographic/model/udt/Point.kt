/**
 * canary-depot
 * Copyright (C) 2017  carddamom

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http:></http:>//www.gnu.org/licenses/>.
 */
package cc.cinnamon.canary.geographic.model.udt

import cc.cinnamon.canary.geographic.model.Public
import cc.cinnamon.canary.geographic.model.udt.records.PointRecord
import org.jooq.Schema
import org.jooq.UDTField
import org.jooq.impl.UDTImpl

import java.math.BigDecimal

class Point
/**
 * No further instances allowed
 */
private constructor() : UDTImpl<PointRecord>("point", null) {

    /**
     * The class holding records for this type
     */
    override fun getRecordType(): Class<PointRecord> {
        return PointRecord::class.java
    }

    /**
     * {@inheritDoc}
     */
    override fun getSchema(): Schema {
        return Public.PUBLIC
    }

    companion object {

        private val serialVersionUID: Long = -1501610517

        /**
         * The reference instance of `public.point`
         */
        val POINT = Point()

        /**
         * The attribute `public.point.latitude`.
         */
        val LATITUDE = UDTImpl.createField("latitude",
                org.jooq.impl.SQLDataType.NUMERIC.precision(10, 6), Point.POINT, "")

        /**
         * The attribute `public.point.longitude`.
         */
        val LONGITUDE = UDTImpl.createField("longitude",
                org.jooq.impl.SQLDataType.NUMERIC.precision(10, 6), Point.POINT, "")
    }
}
