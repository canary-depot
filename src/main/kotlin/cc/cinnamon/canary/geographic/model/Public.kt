/**
 * canary-depot
 * Copyright (C) 2017  carddamom

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http:></http:>//www.gnu.org/licenses/>.
 */
package cc.cinnamon.canary.geographic.model

import org.jooq.Catalog
import org.jooq.impl.SchemaImpl

class Public
/**
 * No further instances allowed
 */
private constructor() : SchemaImpl("public", null) {

    /**
     * {@inheritDoc}
     */
    override fun getCatalog(): Catalog {
        return DefaultCatalog.DEFAULT_CATALOG
    }

    companion object {

        private val serialVersionUID: Long = -1832803132

        /**
         * The reference instance of `public`
         */
        val PUBLIC = Public()
    }
}
