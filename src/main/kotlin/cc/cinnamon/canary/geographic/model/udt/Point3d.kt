/**
 * canary-depot
 * Copyright (C) 2017  carddamom

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http:></http:>//www.gnu.org/licenses/>.
 */
package cc.cinnamon.canary.geographic.model.udt

import cc.cinnamon.canary.geographic.model.Public
import cc.cinnamon.canary.geographic.model.udt.records.Point3dRecord
import org.jooq.Schema
import org.jooq.UDTField
import org.jooq.impl.UDTImpl

import java.math.BigDecimal

class Point3d
/**
 * No further instances allowed
 */
private constructor() : UDTImpl<Point3dRecord>("point3d", null) {

    /**
     * The class holding records for this type
     */
    override fun getRecordType(): Class<Point3dRecord> {
        return Point3dRecord::class.java
    }

    /**
     * {@inheritDoc}
     */
    override fun getSchema(): Schema {
        return Public.PUBLIC
    }

    companion object {

        private val serialVersionUID: Long = -961923613

        /**
         * The reference instance of `public.point3d`
         */
        val POINT3D = Point3d()

        /**
         * The attribute `public.point3d.latitude`.
         */
        val LATITUDE = UDTImpl.createField("latitude",
                org.jooq.impl.SQLDataType.NUMERIC.precision(10, 6), Point3d.POINT3D, "")

        /**
         * The attribute `public.point3d.longitude`.
         */
        val LONGITUDE = UDTImpl.createField("longitude",
                org.jooq.impl.SQLDataType.NUMERIC.precision(10, 6), Point3d.POINT3D, "")

        /**
         * The attribute `public.point3d.altitude`.
         */
        val ALTITUDE = UDTImpl.createField("altitude",
                org.jooq.impl.SQLDataType.NUMERIC.precision(8, 3), Point3d.POINT3D, "")
    }
}
