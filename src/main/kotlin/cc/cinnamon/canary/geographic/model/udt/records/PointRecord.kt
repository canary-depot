/**
 * canary-depot
 * Copyright (C) 2017  carddamom

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http:></http:>//www.gnu.org/licenses/>.
 */
package cc.cinnamon.canary.geographic.model.udt.records

import cc.cinnamon.canary.geographic.model.udt.Point
import cc.cinnamon.canary.geographic.model.udt.interfaces.IPoint
import org.jooq.Field
import org.jooq.Record2
import org.jooq.Row2
import org.jooq.impl.UDTRecordImpl

import java.math.BigDecimal

class PointRecord : UDTRecordImpl<PointRecord>, Record2<BigDecimal, BigDecimal>, IPoint {

    /**
     * Getter for `public.point.latitude`.
     */
    /**
     * Setter for `public.point.latitude`.
     */
    override var latitude: BigDecimal
        get() = this.get(0) as BigDecimal
        set(value) = this.set(0, value)

    /**
     * Getter for `public.point.longitude`.
     */
    /**
     * Setter for `public.point.longitude`.
     */
    override var longitude: BigDecimal
        get() = this.get(1) as BigDecimal
        set(value) = this.set(1, value)

    // -------------------------------------------------------------------------
    // Record2 type implementation
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    override fun fieldsRow(): Row2<BigDecimal, BigDecimal> {
        return super.fieldsRow() as Row2<*, *>
    }

    /**
     * {@inheritDoc}
     */
    override fun valuesRow(): Row2<BigDecimal, BigDecimal> {
        return super.valuesRow() as Row2<*, *>
    }

    /**
     * {@inheritDoc}
     */
    override fun field1(): Field<BigDecimal> {
        return Point.LATITUDE
    }

    /**
     * {@inheritDoc}
     */
    override fun field2(): Field<BigDecimal> {
        return Point.LONGITUDE
    }

    /**
     * {@inheritDoc}
     */
    override fun value1(): BigDecimal {
        return this.latitude
    }

    /**
     * {@inheritDoc}
     */
    override fun value2(): BigDecimal {
        return this.longitude
    }

    /**
     * {@inheritDoc}
     */
    override fun value1(value: BigDecimal): PointRecord {
        this.latitude = value
        return this
    }

    /**
     * {@inheritDoc}
     */
    override fun value2(value: BigDecimal): PointRecord {
        this.longitude = value
        return this
    }

    /**
     * {@inheritDoc}
     */
    override fun values(value1: BigDecimal, value2: BigDecimal): PointRecord {
        this.value1(value1)
        this.value2(value2)
        return this
    }

    // -------------------------------------------------------------------------
    // FROM and INTO
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    override fun from(from: IPoint) {
        this.latitude = from.latitude
        this.longitude = from.longitude
    }

    /**
     * {@inheritDoc}
     */
    override fun <E : IPoint> into(into: E): E {
        into.from(this)
        return into
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached PointRecord
     */
    constructor() : super(Point.POINT) {
    }

    /**
     * Create a detached, initialised PointRecord
     */
    constructor(latitude: BigDecimal, longitude: BigDecimal) : super(Point.POINT) {

        this.set(0, latitude)
        this.set(1, longitude)
    }

    companion object {

        private val serialVersionUID: Long = -1269307413
    }
}
