/**
 * canary-depot
 * Copyright (C) 2017  carddamom

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http:></http:>//www.gnu.org/licenses/>.
 */
package cc.cinnamon.canary.geographic.model.udt.pojos

import cc.cinnamon.canary.geographic.model.udt.interfaces.IPoint3d

import java.math.BigDecimal

class Point3d : IPoint3d {

    override var latitude: BigDecimal? = null
    override var longitude: BigDecimal? = null
    override var altitude: BigDecimal? = null

    constructor() {}

    constructor(value: Point3d) {
        this.latitude = value.latitude
        this.longitude = value.longitude
        this.altitude = value.altitude
    }

    constructor(latitude: BigDecimal, longitude: BigDecimal, altitude: BigDecimal) {
        this.latitude = latitude
        this.longitude = longitude
        this.altitude = altitude
    }

    override fun equals(obj: Any?): Boolean {
        if (this === obj) {
            return true
        }
        if (obj == null) {
            return false
        }
        if (this.javaClass != obj.javaClass) {
            return false
        }
        val other = obj as Point3d?
        if (this.latitude == null) {
            if (other!!.latitude != null) {
                return false
            }
        } else if (this.latitude != other!!.latitude) {
            return false
        }
        if (this.longitude == null) {
            if (other.longitude != null) {
                return false
            }
        } else if (this.longitude != other.longitude) {
            return false
        }
        if (this.altitude == null) {
            if (other.altitude != null) {
                return false
            }
        } else if (this.altitude != other.altitude) {
            return false
        }
        return true
    }

    override fun hashCode(): Int {
        val prime = 31
        var result = 1
        result = prime * result + if (this.latitude == null) 0 else this.latitude!!.hashCode()
        result = prime * result + if (this.longitude == null) 0 else this.longitude!!.hashCode()
        result = prime * result + if (this.altitude == null) 0 else this.altitude!!.hashCode()
        return result
    }

    override fun toString(): String {
        val sb = StringBuilder("Point3d (")

        sb.append(this.latitude)
        sb.append(", ").append(this.longitude)
        sb.append(", ").append(this.altitude)

        sb.append(")")
        return sb.toString()
    }

    // -------------------------------------------------------------------------
    // FROM and INTO
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    override fun from(from: IPoint3d) {
        this.latitude = from.latitude
        this.longitude = from.longitude
        this.altitude = from.altitude
    }

    /**
     * {@inheritDoc}
     */
    override fun <E : IPoint3d> into(into: E): E {
        into.from(this)
        return into
    }

    companion object {

        private val serialVersionUID: Long = 1784165339
    }
}
