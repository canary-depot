/**
 * canary-depot
 * Copyright (C) 2017  carddamom

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http:></http:>//www.gnu.org/licenses/>.
 */
package cc.cinnamon.canary.geographic.model.udt.records

import cc.cinnamon.canary.geographic.model.udt.Point3d
import cc.cinnamon.canary.geographic.model.udt.interfaces.IPoint3d
import org.jooq.Field
import org.jooq.Record3
import org.jooq.Row3
import org.jooq.impl.UDTRecordImpl

import java.math.BigDecimal

class Point3dRecord : UDTRecordImpl<Point3dRecord>, Record3<BigDecimal, BigDecimal, BigDecimal>, IPoint3d {

    /**
     * Getter for `public.point3d.latitude`.
     */
    /**
     * Setter for `public.point3d.latitude`.
     */
    override var latitude: BigDecimal
        get() = this.get(0) as BigDecimal
        set(value) = this.set(0, value)

    /**
     * Getter for `public.point3d.longitude`.
     */
    /**
     * Setter for `public.point3d.longitude`.
     */
    override var longitude: BigDecimal
        get() = this.get(1) as BigDecimal
        set(value) = this.set(1, value)

    /**
     * Getter for `public.point3d.altitude`.
     */
    /**
     * Setter for `public.point3d.altitude`.
     */
    override var altitude: BigDecimal
        get() = this.get(2) as BigDecimal
        set(value) = this.set(2, value)

    // -------------------------------------------------------------------------
    // Record3 type implementation
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    override fun fieldsRow(): Row3<BigDecimal, BigDecimal, BigDecimal> {
        return super.fieldsRow() as Row3<*, *, *>
    }

    /**
     * {@inheritDoc}
     */
    override fun valuesRow(): Row3<BigDecimal, BigDecimal, BigDecimal> {
        return super.valuesRow() as Row3<*, *, *>
    }

    /**
     * {@inheritDoc}
     */
    override fun field1(): Field<BigDecimal> {
        return Point3d.LATITUDE
    }

    /**
     * {@inheritDoc}
     */
    override fun field2(): Field<BigDecimal> {
        return Point3d.LONGITUDE
    }

    /**
     * {@inheritDoc}
     */
    override fun field3(): Field<BigDecimal> {
        return Point3d.ALTITUDE
    }

    /**
     * {@inheritDoc}
     */
    override fun value1(): BigDecimal {
        return this.latitude
    }

    /**
     * {@inheritDoc}
     */
    override fun value2(): BigDecimal {
        return this.longitude
    }

    /**
     * {@inheritDoc}
     */
    override fun value3(): BigDecimal {
        return this.altitude
    }

    /**
     * {@inheritDoc}
     */
    override fun value1(value: BigDecimal): Point3dRecord {
        this.latitude = value
        return this
    }

    /**
     * {@inheritDoc}
     */
    override fun value2(value: BigDecimal): Point3dRecord {
        this.longitude = value
        return this
    }

    /**
     * {@inheritDoc}
     */
    override fun value3(value: BigDecimal): Point3dRecord {
        this.altitude = value
        return this
    }

    /**
     * {@inheritDoc}
     */
    override fun values(value1: BigDecimal, value2: BigDecimal, value3: BigDecimal): Point3dRecord {
        this.value1(value1)
        this.value2(value2)
        this.value3(value3)
        return this
    }

    // -------------------------------------------------------------------------
    // FROM and INTO
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    override fun from(from: IPoint3d) {
        this.latitude = from.latitude
        this.longitude = from.longitude
        this.altitude = from.altitude
    }

    /**
     * {@inheritDoc}
     */
    override fun <E : IPoint3d> into(into: E): E {
        into.from(this)
        return into
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached Point3dRecord
     */
    constructor() : super(Point3d.POINT3D) {
    }

    /**
     * Create a detached, initialised Point3dRecord
     */
    constructor(latitude: BigDecimal, longitude: BigDecimal, altitude: BigDecimal) : super(Point3d.POINT3D) {

        this.set(0, latitude)
        this.set(1, longitude)
        this.set(2, altitude)
    }

    companion object {

        private val serialVersionUID: Long = -10948819
    }
}
