/**
 * canary-depot
 * Copyright (C) 2017  carddamom

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http:></http:>//www.gnu.org/licenses/>.
 */
package cc.cinnamon.canary.geographic.model.udt.pojos

import cc.cinnamon.canary.geographic.model.udt.interfaces.IPoint

import java.math.BigDecimal

class Point : IPoint {

    override var latitude: BigDecimal? = null
    override var longitude: BigDecimal? = null

    constructor() {}

    constructor(value: Point) {
        this.latitude = value.latitude
        this.longitude = value.longitude
    }

    constructor(latitude: BigDecimal, longitude: BigDecimal) {
        this.latitude = latitude
        this.longitude = longitude
    }

    override fun equals(obj: Any?): Boolean {
        if (this === obj) {
            return true
        }
        if (obj == null) {
            return false
        }
        if (this.javaClass != obj.javaClass) {
            return false
        }
        val other = obj as Point?
        if (this.latitude == null) {
            if (other!!.latitude != null) {
                return false
            }
        } else if (this.latitude != other!!.latitude) {
            return false
        }
        if (this.longitude == null) {
            if (other.longitude != null) {
                return false
            }
        } else if (this.longitude != other.longitude) {
            return false
        }
        return true
    }

    override fun hashCode(): Int {
        val prime = 31
        var result = 1
        result = prime * result + if (this.latitude == null) 0 else this.latitude!!.hashCode()
        result = prime * result + if (this.longitude == null) 0 else this.longitude!!.hashCode()
        return result
    }

    override fun toString(): String {
        val sb = StringBuilder("Point (")

        sb.append(this.latitude)
        sb.append(", ").append(this.longitude)

        sb.append(")")
        return sb.toString()
    }

    // -------------------------------------------------------------------------
    // FROM and INTO
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    override fun from(from: IPoint) {
        this.latitude = from.latitude
        this.longitude = from.longitude
    }

    /**
     * {@inheritDoc}
     */
    override fun <E : IPoint> into(into: E): E {
        into.from(this)
        return into
    }

    companion object {

        private val serialVersionUID: Long = 1645499876
    }
}
