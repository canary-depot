/**
 * canary-depot
 * Copyright (C) 2017  carddamom

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http:></http:>//www.gnu.org/licenses/>.
 */
package cc.cinnamon.canary.projects.model

import cc.cinnamon.canary.projects.model.tables.License
import cc.cinnamon.canary.projects.model.tables.Project
import org.jooq.ForeignKey
import org.jooq.Identity
import org.jooq.Record
import org.jooq.UniqueKey
import org.jooq.impl.AbstractKeys

object Keys {

    // -------------------------------------------------------------------------
    // IDENTITY definitions
    // -------------------------------------------------------------------------

    val IDENTITY_LICENSE = Identities0.IDENTITY_LICENSE
    val IDENTITY_PROJECT = Identities0.IDENTITY_PROJECT

    // -------------------------------------------------------------------------
    // UNIQUE and PRIMARY KEY definitions
    // -------------------------------------------------------------------------

    val LICENSE_PKEY = UniqueKeys0.LICENSE_PKEY
    val PROJECT_PKEY = UniqueKeys0.PROJECT_PKEY
    val PROJECT_NAME_KEY = UniqueKeys0.PROJECT_NAME_KEY

    // -------------------------------------------------------------------------
    // FOREIGN KEY definitions
    // -------------------------------------------------------------------------

    val PROJECT__PROJECT_LICENSE_ID_FKEY = ForeignKeys0.PROJECT__PROJECT_LICENSE_ID_FKEY

    // -------------------------------------------------------------------------
    // [#1459] distribute members to avoid static initialisers > 64kb
    // -------------------------------------------------------------------------

    private class Identities0 : AbstractKeys() {
        companion object {
            var IDENTITY_LICENSE = AbstractKeys.createIdentity(License.LICENSE,
                    License.LICENSE.ID)
            var IDENTITY_PROJECT = AbstractKeys.createIdentity(Project.PROJECT,
                    Project.PROJECT.ID)
        }
    }

    private class UniqueKeys0 : AbstractKeys() {
        companion object {
            val LICENSE_PKEY = AbstractKeys.createUniqueKey(License.LICENSE,
                    "license_pkey", License.LICENSE.ID)
            val PROJECT_PKEY = AbstractKeys.createUniqueKey(Project.PROJECT,
                    "project_pkey", Project.PROJECT.ID)
            val PROJECT_NAME_KEY = AbstractKeys.createUniqueKey(Project.PROJECT,
                    "project_name_key", Project.PROJECT.NAME)
        }
    }

    private class ForeignKeys0 : AbstractKeys() {
        companion object {
            val PROJECT__PROJECT_LICENSE_ID_FKEY = AbstractKeys.createForeignKey(
                    Keys.LICENSE_PKEY, Project.PROJECT, "project__project_license_id_fkey",
                    Project.PROJECT.LICENSE_ID)
        }
    }
}
