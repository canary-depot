/*
 * This file is generated by jOOQ.
*/
package cc.cinnamon.canary.projects.model.tables.interfaces


import javax.annotation.Generated
import javax.validation.constraints.Size
import java.io.Serializable


/**
 * This class is generated by jOOQ.
 */
@Generated(value = *arrayOf("http://www.jooq.org", "jOOQ version:3.9.1", "schema version:public_4"), date = "2017-02-20T21:57:05.285Z", comments = "This class is generated by jOOQ")
interface ILicense : Serializable {

    /**
     * Getter for `public.license.id`.
     */
    /**
     * Setter for `public.license.id`.
     */
    var id: Long?

    /**
     * Getter for `public.license.name`.
     */
    /**
     * Setter for `public.license.name`.
     */
    @get:Size(max = 10)
    var name: String

    /**
     * Getter for `public.license.url`.
     */
    /**
     * Setter for `public.license.url`.
     */
    @get:Size(max = 120)
    var url: String

    /**
     * Getter for `public.license.description`.
     */
    /**
     * Setter for `public.license.description`.
     */
    var description: String

    // -------------------------------------------------------------------------
    // FROM and INTO
    // -------------------------------------------------------------------------

    /**
     * Load data from another generated Record/POJO implementing the common interface ILicense
     */
    fun from(from: ILicense)

    /**
     * Copy data into another generated Record/POJO implementing the common interface ILicense
     */
    fun <E : ILicense> into(into: E): E
}
