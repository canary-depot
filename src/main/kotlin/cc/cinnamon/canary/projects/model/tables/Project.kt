/**
 * canary-depot
 * Copyright (C) 2017  carddamom

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http:></http:>//www.gnu.org/licenses/>.
 */
package cc.cinnamon.canary.projects.model.tables

import cc.cinnamon.canary.projects.model.Keys
import cc.cinnamon.canary.projects.model.Public
import org.jooq.*
import org.jooq.impl.TableImpl

import java.sql.Date
import java.util.Arrays

class Project private constructor(alias: String, aliased: Table<Record>, parameters: Array<Field<*>>? = null) : TableImpl<Record>(alias, null, aliased, parameters, "") {

    /**
     * The class holding records for this type
     */
    override fun getRecordType(): Class<Record> {
        return Record::class.java
    }

    /**
     * The column `public.project.id`.
     */
    val ID = AbstractTable.createField("id",
            org.jooq.impl.SQLDataType.BIGINT.nullable(false).defaultValue(
                    org.jooq.impl.DSL.field("nextval('project_id_seq'::regclass)", org.jooq.impl.SQLDataType.BIGINT)),
            this, "")

    /**
     * The column `public.project.name`.
     */
    val NAME = AbstractTable.createField("name",
            org.jooq.impl.SQLDataType.VARCHAR.length(200).nullable(false), this, "")

    /**
     * The column `public.project.description`.
     */
    val DESCRIPTION = AbstractTable.createField("description",
            org.jooq.impl.SQLDataType.CLOB, this, "")

    /**
     * The column `public.project.homepage`.
     */
    val HOMEPAGE = AbstractTable.createField("homepage",
            org.jooq.impl.SQLDataType.VARCHAR.length(120), this, "")

    /**
     * The column `public.project.created`.
     */
    val CREATED = AbstractTable.createField("created",
            org.jooq.impl.SQLDataType.DATE, this, "")

    /**
     * The column `public.project.shortdesc`.
     */
    val SHORTDESC = AbstractTable.createField("shortdesc",
            org.jooq.impl.SQLDataType.VARCHAR.length(200), this, "")

    /**
     * The column `public.project.category`.
     */
    val CATEGORY = AbstractTable.createField("category",
            org.jooq.impl.SQLDataType.VARCHAR.length(60), this, "")

    /**
     * The column `public.project.language`.
     */
    val LANGUAGE = AbstractTable.createField("language",
            org.jooq.impl.SQLDataType.VARCHAR.length(20), this, "")

    /**
     * The column `public.project.license_id`.
     */
    val LICENSE_ID = AbstractTable.createField("license_id",
            org.jooq.impl.SQLDataType.BIGINT, this, "")

    /**
     * Create a `public.project` table reference
     */
    constructor() : this("project", null) {
    }

    /**
     * Create an aliased `public.project` table reference
     */
    constructor(alias: String) : this(alias, Project.PROJECT) {
    }

    /**
     * {@inheritDoc}
     */
    override fun getSchema(): Schema {
        return Public.PUBLIC
    }

    /**
     * {@inheritDoc}
     */
    override fun getIdentity(): Identity<Record, Long> {
        return Keys.IDENTITY_PROJECT
    }

    /**
     * {@inheritDoc}
     */
    override fun getPrimaryKey(): UniqueKey<Record> {
        return Keys.PROJECT_PKEY
    }

    /**
     * {@inheritDoc}
     */
    override fun getKeys(): List<UniqueKey<Record>> {
        return Arrays.asList(Keys.PROJECT_PKEY, Keys.PROJECT_NAME_KEY)
    }

    /**
     * {@inheritDoc}
     */
    override fun getReferences(): List<ForeignKey<Record, *>> {
        return Arrays.asList<ForeignKey<Record, *>>(Keys.PROJECT__PROJECT_LICENSE_ID_FKEY)
    }

    /**
     * {@inheritDoc}
     */
    override fun `as`(alias: String): Project {
        return Project(alias, this)
    }

    /**
     * Rename this table
     */
    override fun rename(name: String): Project {
        return Project(name, null)
    }

    companion object {

        private val serialVersionUID: Long = -698665837

        /**
         * The reference instance of `public.project`
         */
        val PROJECT = Project()
    }
}
