package cc.cinnamon.canary.utilities.exceptions

class InvalidRequestParameter : Exception() {
    companion object {

        private val serialVersionUID = 2929012845141454429L
    }

}
