package cc.cinnamon.canary.utilities.exceptions

import com.google.common.base.MoreObjects
import com.google.common.base.Objects

class ResourceNotFound(val objectId: Long?) : Exception() {

    override fun toString(): String {
        return MoreObjects.toStringHelper(this).add("super", super.toString()).add("objectId", objectId).toString()
    }

    override fun hashCode(): Int {
        return Objects.hashCode(super.hashCode(), objectId)
    }

    override fun equals(`object`: Any?): Boolean {
        if (`object` is ResourceNotFound) {
            if (!super.equals(`object`))
                return false
            return Objects.equal(this.objectId, `object`.objectId)
        }
        return false
    }

    companion object {

        private val serialVersionUID = -3928670096418374520L
    }

}
