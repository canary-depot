package cc.cinnamon.canary.utilities.rest

import com.google.common.base.MoreObjects
import com.google.common.base.Objects

class RestListEnvelope<T> : AbstractRestEnvelope<List<T>> {

    var size: Int = 0

    private constructor(builder: Builder<T>) : super(builder.data, builder.errorCode, builder.errorMessage) {
        this.size = builder.size
    }

    constructor(data: List<T>, errorCode: Int, errorMessage: String) : super(data, errorCode, errorMessage) {
        this.size = data.size
    }

    constructor() : super() {}

    override fun hashCode(): Int {
        return Objects.hashCode(super.hashCode(), size)
    }

    override fun equals(`object`: Any?): Boolean {
        if (`object` is RestListEnvelope<*>) {
            if (!super.equals(`object`))
                return false
            return Objects.equal(this.size, `object`.size)
        }
        return false
    }

    override fun toString(): String {
        return MoreObjects.toStringHelper(this).add("super", super.toString()).add("size", size).toString()
    }

    class Builder<R> private constructor() {
        private var size: Int = 0
        private var data: List<R>? = null
        private var errorCode: Int = 0
        private var errorMessage: String? = null

        fun withSize(size: Int): Builder<R> {
            this.size = size
            return this
        }

        fun withData(data: List<R>): Builder<R> {
            this.data = data
            return this
        }

        fun withErrorCode(errorCode: Int): Builder<R> {
            this.errorCode = errorCode
            return this
        }

        fun withErrorMessage(errorMessage: String): Builder<R> {
            this.errorMessage = errorMessage
            return this
        }

        fun build(): RestListEnvelope<R> {
            return RestListEnvelope(this)
        }
    }

    companion object {

        private val serialVersionUID = 1L

        fun <R> builder(): Builder<R> {
            return Builder()
        }
    }

}
