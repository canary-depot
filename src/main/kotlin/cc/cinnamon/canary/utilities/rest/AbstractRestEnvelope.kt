package cc.cinnamon.canary.utilities.rest

import com.google.common.base.MoreObjects
import com.google.common.base.Objects

import java.io.Serializable

abstract class AbstractRestEnvelope<T> : Serializable {

    var data: T? = null
    var errorCode: Int = 0
    var errorMessage: String? = null

    protected constructor(data: T, errorCode: Int, errorMessage: String) {
        this.data = data
        this.errorCode = errorCode
        this.errorMessage = errorMessage
    }

    protected constructor() {}

    override fun toString(): String {
        return MoreObjects.toStringHelper(this).add("data", data).add("errorCode", errorCode)
                .add("errorMessage", errorMessage).toString()
    }

    override fun hashCode(): Int {
        return Objects.hashCode(data, errorCode, errorMessage)
    }

    override fun equals(`object`: Any?): Boolean {
        if (`object` is AbstractRestEnvelope<*>) {
            val that = `object`
            return Objects.equal(this.data, that.data) && Objects.equal(this.errorCode, that.errorCode)
                    && Objects.equal(this.errorMessage, that.errorMessage)
        }
        return false
    }

    companion object {

        private const val serialVersionUID = -1710927267011157690L
    }


}
