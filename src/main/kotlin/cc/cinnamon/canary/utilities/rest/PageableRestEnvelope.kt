package cc.cinnamon.canary.utilities.rest

import com.google.common.base.MoreObjects
import com.google.common.base.Objects

class PageableRestEnvelope<T> : AbstractRestEnvelope<List<T>> {

    var page: Int = 0

    var lastId: Int = 0

    var size: Int = 0

    private constructor(builder: Builder<T>) : super(builder.data, builder.errorCode, builder.errorMessage) {
        this.page = builder.page
        this.lastId = builder.lastId
        this.size = builder.size
    }

    constructor(data: List<T>, errorCode: Int, errorMessage: String, page: Int, lastId: Int, size: Int) : super(data, errorCode, errorMessage) {
        this.page = page
        this.lastId = lastId
        this.size = size
    }

    constructor() {}

    override fun hashCode(): Int {
        return Objects.hashCode(super.hashCode(), page, lastId, size)
    }

    override fun equals(`object`: Any?): Boolean {
        if (`object` is PageableRestEnvelope<*>) {
            if (!super.equals(`object`))
                return false
            val that = `object`
            return Objects.equal(this.page, that.page) && Objects.equal(this.lastId, that.lastId)
                    && Objects.equal(this.size, that.size)
        }
        return false
    }

    override fun toString(): String {
        return MoreObjects.toStringHelper(this).add("super", super.toString()).add("page", page)
                .add("lastId", lastId).add("size", size).toString()
    }

    class Builder<R> private constructor() {
        private var page: Int = 0
        private var lastId: Int = 0
        private var size: Int = 0
        private var data: List<R>? = null
        private var errorCode: Int = 0
        private var errorMessage: String? = null

        fun withPage(page: Int): Builder<R> {
            this.page = page
            return this
        }

        fun withLastId(lastId: Int): Builder<R> {
            this.lastId = lastId
            return this
        }

        fun withSize(size: Int): Builder<R> {
            this.size = size
            return this
        }

        fun withData(data: List<R>): Builder<R> {
            this.data = data
            return this
        }

        fun withErrorCode(errorCode: Int): Builder<R> {
            this.errorCode = errorCode
            return this
        }

        fun withErrorMessage(errorMessage: String): Builder<R> {
            this.errorMessage = errorMessage
            return this
        }

        fun build(): PageableRestEnvelope<R> {
            return PageableRestEnvelope(this)
        }
    }

    companion object {

        private val serialVersionUID = -4576101760524561156L

        fun <R> builder(): Builder<R> {
            return Builder()
        }
    }

}
