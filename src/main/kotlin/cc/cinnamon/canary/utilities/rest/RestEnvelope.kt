package cc.cinnamon.canary.utilities.rest

class RestEnvelope<T> : AbstractRestEnvelope<T> {

    private constructor(builder: Builder<T>) : super(builder.data, builder.errorCode, builder.errorMessage) {}

    constructor(data: T, errorCode: Int, errorMessage: String) : super(data, errorCode, errorMessage) {}

    constructor() : super() {}

    class Builder<R> private constructor() {
        private var data: R? = null
        private var errorCode: Int = 0
        private var errorMessage: String? = null

        fun withData(data: R): Builder<R> {
            this.data = data
            return this
        }

        fun withErrorCode(errorCode: Int): Builder<R> {
            this.errorCode = errorCode
            return this
        }

        fun withErrorMessage(errorMessage: String): Builder<R> {
            this.errorMessage = errorMessage
            return this
        }

        fun build(): RestEnvelope<R> {
            return RestEnvelope(this)
        }
    }

    companion object {

        private val serialVersionUID = 6426760113089674989L

        fun <R> builder(): Builder<R> {
            return Builder()
        }
    }

}
